﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinalProject
{
    /// <summary>
    /// Interaction logic for AdminMainWindow.xaml
    /// </summary>
    public partial class AdminMainWindow : Page
    { 
        
        public AdminMainWindow()
        {
            InitializeComponent();
        }
        public void NavigateToAdmin1(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("Admin1.xaml", UriKind.Relative);
            this.NavigationService.Navigate(uri);
        }
        public void NavigateToAdmin4(object sender, RoutedEventArgs e)
        {
            
                Uri uri = new Uri("Admin4.xaml", UriKind.Relative);
                this.NavigationService.Navigate(uri);
               
            
          
            
        }
        public void NavigateToAdmin5(object sender, RoutedEventArgs e)
        {
            Uri uri = new Uri("Admin5.xaml", UriKind.Relative);
            this.NavigationService.Navigate(uri);
        }

    }
}
