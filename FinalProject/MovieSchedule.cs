﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FinalProject
{
    class MovieSchedule
    {
        static MovieSchedule()
        {
            createTable();
        }

        public static ObservableCollection<MovieSchedule> ms = new ObservableCollection<MovieSchedule>();
        public event PropertyChangedEventHandler PropertyChanged;

        public int Id { get; set; }



        //defining name property with getter and setter

        string name;
        public string Name 
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                    MessageBox.Show("Updated Customer Detail" + "\n\n" + "Name:" + this.name);
                }
            }
        }



        //defining screen property with getter and setter
        int screen;
        public int Screen 
        {
            get
            {
                return screen;
            }
            set
            {
                screen = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Screen"));
                    MessageBox.Show("Updated Customer Detail" + "\n\n" + "Name:" + this.name);
                }
            }
        }



        //defining movietype property with getter and setter

        string movietype;
        public string MovieType
        {
            get
            {
                return movietype;
            }
            set
            {
                movietype = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("MovieType"));
                    MessageBox.Show("Updated Customer Detail" + "\n\n" + "Name:" + this.name);
                }
            }
        }


        //defining showtimes property with getter and setter

        string showtimes;
        public string ShowTimes
        {
            get
            {
                return showtimes;
            }
            set
            {
                showtimes = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("ShowTimes"));
                    MessageBox.Show("Updated Customer Detail" + "\n\n" + "Name:" + this.name);
                }
            }
        }



        //save function. for datagrid

        public void save(int id, string name, int screen, string movietype,string showtimes)
        {
            this.Id = id;
            this.Name = name;
            this.Screen = screen;
            this.MovieType = movietype;
            this.ShowTimes = showtimes;
            ms.Add(this);
        }


        //making datatable object

        public static DataTable movieTable = new DataTable();       





        //creating table

        public static void createTable()
        {
            movieTable.Columns.Add("Id", typeof(int));
            movieTable.Columns.Add("Name", typeof(string));
            movieTable.Columns.Add("Screen", typeof(int));
            movieTable.Columns.Add("MovieType", typeof(string));
            movieTable.Columns.Add("ShowTimes", typeof(string));
            movieTable.PrimaryKey = new DataColumn[] { movieTable.Columns["Id"] };
            movieTable.AcceptChanges();
            movieTable.RowChanged += new DataRowChangeEventHandler(Row_Changed);
        }






        //no need to add a show as humara cinema bohat chota hai and limited number of screens hain... xD



        /*public void addToTable(int id, string name, int screen, string movietype, string showtimes)
        {
            movieTable.Rows.Add(id, name, screen, movietype, showtimes);
            updateDB();
        }*/





        //if row changed

        private static void Row_Changed(object sender, DataRowChangeEventArgs e)
        {
            string customerDetails = "";
            foreach (DataRow r in movieTable.Rows)
            {
                customerDetails += r["Id"] + " " + r["Name"] + " " + r["Screen"] + " " +r["MovieType"] + " " + r["ShowTimes"] + "\n";
            }
            //MessageBox.Show(customerDetails);
        //    updateDB();
        }




        //sqladapter object
        //updating database if change in datatable is made. 


        public static SqlDataAdapter sda;
      /*  public static void updateDB()
        {
            
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda); //what id updatedb doifng?   ye saray 2 way binding ki hain... agar datatable me change perform karu, wo database me bhi update hojaye.okay.
                sda.Update(movieTable);      
                 

        }*/
        

        //connecting datatable with database.

       
        public static void getData()
        {
            try
            {
                //open a connection to the database
               // DB_userdata.conn.Open(); //is jaga per.
                //SQL statement used against the database
                string query = "SELECT * FROM Movie_schedule";
                //execute the SQL statement against the database
                SqlCommand command = new SqlCommand(query, DB_userdata.conn);  //sirf isi jaga error hai kia? iguess. because almost saray exceptions idhar hi atay hain.
                //create a bridge between DataTable and database
                sda = new SqlDataAdapter(command);
                //fill DataTable with data
                sda.Fill(movieTable);
                movieTable.AcceptChanges();
                movieTable.RowChanged += new DataRowChangeEventHandler(Row_Changed);
               
            }
            catch (System.Data.SqlClient.SqlException sqlException)
            {
                MessageBox.Show(sqlException.Message);
            }
            
        }




    }
}
