﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FinalProject
{
    class User :INotifyPropertyChanged
    {

        static User()
        {
            createTable();
        }
            public string TypeofUser { get; set; }
            //public string Name { get; set; } 
            public string Email { get; set; }
            //public string Pass { get; set; }
            //public string RePass { get; set; }

           public static ObservableCollection<User> users = new ObservableCollection<User>();

                 public event PropertyChangedEventHandler PropertyChanged;
                string name;
                public string Name //and here no? maybe wait.
                {
                    get
                    {
                        return name;
                    }
                    set
                    {
                        name = value;
                        if (PropertyChanged != null)
                        {
                            PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                            MessageBox.Show("Updated Customer Detail" + "\n\n" + "Name:" + this.name);
                        }
                    }
                }

                string password;
                public string Password
                {
                    get
                    {
                        return password;
                    }
                    set
                    {
                        password = value;
                        if (PropertyChanged != null)
                        {
                            PropertyChanged(this, new PropertyChangedEventArgs("Password"));
                            MessageBox.Show("Updated Customer Detail" + "\n\n" + "Name:" + this.name);
                        }
                    }
                }

                string repassword;
                public string RePassword
                {
                    get
                    {
                        return repassword;
                    }
                    set
                    {
                        repassword = value;
                        if (PropertyChanged != null)
                        {
                            PropertyChanged(this, new PropertyChangedEventArgs("RePassword"));
                            MessageBox.Show("Updated Customer Detail" + "\n\n" + "Name:" + this.name);
                        }
                    }
                }


                public static  DataTable customerTable = new DataTable();

                public static  void createTable()
                {
                    customerTable.Columns.Add("TypeofUser", typeof(string));
                    customerTable.Columns.Add("Name", typeof(string));
                    customerTable.Columns.Add("Email", typeof(string));
                    customerTable.Columns.Add("Password", typeof(string));
                    customerTable.Columns.Add("RePassword", typeof(string));
                    customerTable.PrimaryKey = new DataColumn[] { customerTable.Columns["Email"] };
                    customerTable.AcceptChanges();
                    customerTable.RowChanged += new DataRowChangeEventHandler(Row_Changed);
                }


                public void addToTable(string typeofuser, string name, string email, string pass, string repassword)
                {
                    customerTable.Rows.Add(typeofuser, name, email, password, repassword);
                 //  updateDB();
                }


                private static void Row_Changed(object sender, DataRowChangeEventArgs e)
                {
                    string customerDetails = "";
                    foreach (DataRow r in customerTable.Rows)
                    {
                        customerDetails += r["TypeofUser"] + " " + r["Name"] + " " + r["Email"] + " " +r["Password"] + " " + r["RePassword"]+ "\n";
                    }
                    //MessageBox.Show(customerDetails);
                  //  updateDB();
                }

             /*   public static void updateDB()
                {
                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);
                    sda.Update(customerTable);
                }
        */





            public void save(string typeofuser, string name, string email, string pass, string repassword)
            {
                this.TypeofUser = typeofuser;
                this.Name = name;
                this.Email = email;
                this.Password = pass;
                this.RePassword = repassword; 
                users.Add(this);
            }


            private static SqlDataAdapter sda; 
            public static void getData()
            {
                try
                {
                    //open a connection to the database
                    //DB_userdata.conn.Open();
                    //SQL statement used against the database
                    string query = "SELECT * FROM Customer";
                    //execute the SQL statement against the database
                    SqlCommand command = new SqlCommand(query, DB_userdata.conn);
                    //create a bridge between DataTable and database
                    sda = new SqlDataAdapter(command);
                    //fill DataTable with data
                    sda.Fill(customerTable);
                    customerTable.AcceptChanges();
                    customerTable.RowChanged += new DataRowChangeEventHandler(Row_Changed);
                }
                catch (System.Data.SqlClient.SqlException sqlException)
                {
                    MessageBox.Show(sqlException.Message);
                }
            }


        
    }



}
