﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinalProject
{
    /// <summary>
    /// Interaction logic for SignUpPage.xaml
    /// </summary>
    public partial class SignUpPage : Page
    {
        public SignUpPage()
        {
            InitializeComponent();
        }
        private void SignUpButton(object sender, RoutedEventArgs e)
        {
            string name = ((TextBox)grid.FindName("name")).Text;
            string email = ((TextBox)grid.FindName("email")).Text;
            string password = ((TextBox)grid.FindName("password")).Text;
            string r_password = ((TextBox)grid.FindName("rPassword")).Text;

            bool flag = true;

            if (name.Length < 10)
            {
                flag = false;
            }
            else if (email.Length < 7)
            {
                flag = false;
            }
            else if (password.Length < 8)
            {
                flag = false;
            }
            else if (r_password != password)
            {
                flag = false;
            }

            if (flag == true)//insert query syntax can be found in lecture slides week6-lec1
            {
                //Save data and navigate to next page
            }
            else
            {
                MessageBox.Show("Invalid Data has been entered!\n\n- Name has to be atleast 10 characters long\n- Email has to be atleast 7 characters long\n" +
                    "- Password has to be atleast 8 vharacters long\n- Re-Password and Password have to be the same.");
            }
        }
    }
}
